*** Settings ***
Documentation  This is case for test Share Task
Force Tags  ShareTask
Resource  ../Keywords/ShareTask_key.robot
Resource  ../Keywords/LoginPage_key.robot
Suite Setup  LoginPage_key.Open Website  ${url[0]}  ${browser[1]}
Test Setup  LoginPage_key.Login  ${username[1]}  ${password[0]}
Suite Teardown  LoginPage_key.Close Website


*** Test Cases ***
Check task list
  [Tags]  ShareTaskCase1
  Given Open Shared Tasks
  Then Check Tasks List Name  taskpop
  
Complete task
  [Tags]  ShareTaskCase2
  Given Open Shared Tasks
  When Check Tasks List Name  taskpop
  And Select Checkbox Task  taskpop
  Then Check Complete Task  taskpop
  # ยังไม่เสร็จ

Copy task to other project
  [Tags]  ShareTaskCase3
  Given Open Shared Tasks
  When Check Tasks List Name  taskpop
  And Click Kebab Button
  And Select Copy Task
  And Select Project Name  project2
  






