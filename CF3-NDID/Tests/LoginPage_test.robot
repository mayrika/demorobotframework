*** Settings ***
Documentation  This is case for test Login
Resource  ../Keywords/LoginPage_key.robot
Test Setup  LoginPage_key.Open Website
Test Teardown  LoginPage_key.Close Website



*** Test Cases ***
Login with Username and Password
  LoginPage_key.Input Username  
  LoginPage_key.Input Password
  LoginPage_key.Click Login