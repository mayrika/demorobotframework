*** Settings ***
Library  Selenium2Library
Resource  ../Variables/App/LoginPage_css.robot

*** Keywords ***
Open Website
  [Arguments]  ${url}  ${browser}
  Open Browser  ${url}  ${browser}
  Wait Until Element Is Visible  ${css_TextboxUsername}

Close Website
  Close Browser

Input Username
  Wait Until Element Is Visible  ${css_TextboxUsername}
  Input Text  ${css_TextboxUsername}  ${username}

Input Password
  Wait Until Element Is Visible  ${css_TextboxPassword}
  Input Text  ${css_TextboxPassword}  ${password}

Click Login
  Wait Until Element Is Visible  ${css_LoginBtn}
  Click Button  ${css_LoginBtn}

Login
  [Arguments]  ${username}  ${password}
  Wait Until Element Is Visible  ${css_TextboxUsername}
  Input Text  ${css_TextboxUsername}  ${username}
  Wait Until Element Is Visible  ${css_TextboxPassword}
  Input Text  ${css_TextboxPassword}  ${password}
  Wait Until Element Is Visible  ${css_LoginBtn}
  Click Button  ${css_LoginBtn}
  Wait Until Element Is Visible  ${css_AlertLogin}
  Element Should Be Visible  ${css_AlertLogin}  ${AlertLoginMessage[0]}