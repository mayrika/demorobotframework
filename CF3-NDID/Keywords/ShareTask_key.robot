*** Settings ***
Library  Selenium2Library
Resource  ../Variables/App/ShareTask_css.robot

*** Keywords ***
Open Shared Tasks
  Wait Until Element Is Visible  ${css_ShareTaskTab}
  Click Element  ${css_ShareTaskTab}

Check Tasks List Name
  [Arguments]  ${TaskName}
  Wait Until Element Is Visible  ${css_TaskList}
  Element Should Contain  ${css_TaskList}  ${TaskName}

Select Checkbox Task
  [Arguments]  ${TaskName}
  Click Element  ${css_Checkbox}
  Element Should Not Contain  ${css_Checkbox}  ${TaskName}

Check Complete Task
  [Arguments]  ${TaskName}
  Click Element  ${css_CompleteTask}
  Wait Until Element Is Visible  ${css_CompleteTaskList}
  Element Should Contain  ${css_CompleteTaskList}  ${TaskName}

Click Kebab Button
  Wait Until Element Is Visible  ${css_KebabBtn}
  Click Element  ${css_KebabBtn}
  Wait Until Element Is Visible  ${css_DropdownCopy&State}

Select Copy Task
  Wait Until Element Contains  ${css_DropdownCopy}  ${DropdownCopy}
  Click Element  ${css_DropdownCopy}
  Wait Until Element Is Visible  ${css_CopyTaskModal}

Select Project Name
  [Arguments]  ${ProjectName}
  Select From List By Value  ${css_ProjectList}  ${ProjectName}