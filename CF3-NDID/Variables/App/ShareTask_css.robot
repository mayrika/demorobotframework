*** Settings ***
Library  Selenium2Library

*** Variables ***
#data
${DropdownCopy}  Copy...
#css
${css_ShareTaskTab}  class=shared-task-menu 
${css_TaskList}  class=task-name
${css_Checkbox}  class=ant-checkbox-input
${css_CompleteTask}  class=ant-collapse-header
${css_CompleteTaskList}  class=task-name sc-cmTdod kZjGfY
${css_KebabBtn}  class=option-item-col
${css_DropdownCopy&State}  class=ant-dropdown 
${css_DropdownCopy}  class=option-copy
${css_CopyTaskModal}  class=copy-task-modal
${css_ProjectList}  class=copy-destination-project-selectm