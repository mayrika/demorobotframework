*** Settings ***
Library  Selenium2Library

*** Variables ***
#data
@{url}  https://10.0.1.17
@{browser}  chrome  safari
@{username}  member41@webautomate.com  member43@webautomate.com
@{password}  password
@{AlertLoginMessage}  Login success
#css
${css_TextboxUsername}  id=username
${css_TextboxPassword}  id=password
${css_LoginBtn}  id=submit
${css_AlertLogin}  class=notification-message
${css_AddProjectBtn}  class=add-project-btn